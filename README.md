# Oracle OpTools

The Oracle OpTools is the GPLv3-based toolbox for maintenance scripts for Oracle database instances 
running on Unix-like operating systems. All execution results and gathered data is sent to an ULS server. 
It works for Oracle versions 12c and 19c, multitenant or not. 
See [Universal-Logging-System](www.universal-logging-system.org) for more information about ULS, 
and [ULS-Agent for Oracle Databases](https://www.universal-logging-system.org/dokuwiki/doku.php?id=uls:agents:oracle_tools) 
for more information about this script package. (Although the documentation is going to be moved here)
or find more up-to-date documentation on gitlab [project page](https://gitlab.com/universal-logging-system/Oracle-OpTools).

The scripts have worked also for the 9i, 10g and 11g versions, 
but since I do not have of these versions in use any more, its usage must be checked carefully.

Oracle OpTools is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Oracle OpTools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
